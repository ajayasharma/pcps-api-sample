<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\College;
use App\Http\Resources\CollegeResource;
use App\Http\Requests\CollegeStoreRequest;
use App\Http\Requests\CollegeUpdateRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CollegeController extends Controller
{
    public function __construct()
    {
        // Enable only after the creation of permission and roles, otherwise module will not be accessible
        $this->middleware('permission:college-list');
        $this->middleware('permission:college-create', ['only' => ['store']]);
        $this->middleware('permission:college-edit', ['only' => ['update']]);
        $this->middleware('permission:college-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        Execute only once for creation of permissions and roles.
        $permission1 = Permission::create(['name'=>'college-list']);
        $permission2 = Permission::create(['name'=>'college-create']);
        $permission3 = Permission::create(['name'=>'college-edit']);
        $permission4 = Permission::create(['name'=>'college-delete']);
        $permission5 = Permission::create(['name'=>'college-purge']);

        //$permission1 = Permission::findById(1);
        //$permission2 = Permission::findById(2);
        //$permission3 = Permission::findById(3);
        //$permission4 = Permission::findById(4);
        //$permission5 = Permission::findById(5);

        $role1 = Role::create(['name'=>'College Admin']);
        $role2 = Role::create(['name'=>'College Manager']);
        $role3 = Role::create(['name'=>'College View']);
        $role1->givePermissionTo($permission1);
        $role1->givePermissionTo($permission2);
        $role1->givePermissionTo($permission3);
        $role1->givePermissionTo($permission4);
        $role1->givePermissionTo($permission5);

        $role2->givePermissionTo($permission1);
        $role2->givePermissionTo($permission2);
        $role2->givePermissionTo($permission3);
        $role2->givePermissionTo($permission4);

        $role3->givePermissionTo($permission1);

        $user = auth()->user();
        $user->assignRole('College Admin');*/
        return CollegeResource::collection(College::paginate(25));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CollegeStoreRequest $request)
    {
        $college = College::create($request->all());
        return new CollegeResource($college);
        /*
        OLD CODE FOR REFERENCE
        $college = College::create([
            'college_name'=>$request->college_name,
            'contact_no'=>$request->contact_no,
            'email'=> $request->email,
            'address'=>$request->address,
            'website'=>$request->website
        ]);
        return new CollegeResource($college);*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(College $college)
    {
        /*$user = auth()->user();
        $user->assignRole('Super Admin');
        Permission::create(['name'=>'college-list', 'guard_name'=>'api']);
        Permission::create(['name'=>'college-create', 'guard_name'=>'api']);
        Permission::create(['name'=>'college-edit', 'guard_name'=>'api']);
        Permission::create(['name'=>'college-delete', 'guard_name'=>'api']);
        Permission::create(['name'=>'college-purge', 'guard_name'=>'api']);
        */
        return new CollegeResource($college);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CollegeUpdateRequest $request, $id)
    {
        $college = College::update($request->only(['college_name','contact_no','email','address','website']));
        return new CollegeResource($college);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(College $college)
    {
        $college->delete();
        return response()->json(null, 204);
    }
}
