<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CollegeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request); if all request data is to be passed
        return [
            'id' => $this->id,
            'college_name' => $this->college_name,
            'email' => $this->email,
            'contact_no' => $this->contact_no,
            'address' => $this->address,
            'website' => $this->website
        ];
    }
}
