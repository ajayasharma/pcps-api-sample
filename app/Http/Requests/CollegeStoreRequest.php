<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollegeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'college_name' => 'required|max:255|unique:colleges',
            'contact_no' => 'required|max:10',
            'email' => 'required|email|max:255'
        ];
    }

    public function messages()
    {
        return [
            'college_name.required' => 'College Name is required',
            'college_name.unique' => 'College Name is present',
            'college_name.max' => 'College Name should be maximum of 255',
            'contact_no.required' => 'Contact Number is required',
            'email.required' => 'Email is required'
        ];
    }
}
