<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    use HasApiTokens;
    protected $fillable = [
        'college_name', 'email', 'contact_no', 'website','address'
    ];
}
